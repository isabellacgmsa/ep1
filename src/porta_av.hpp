#ifndef PORTA_AV_HPP
#define PORTA_AV_HPP
#include <string>
#include "barcos.hpp"

using namespace std;

class Porta_av : private Barcos{  //classe filha numero 3
    public:

    private:
        Porta_av();
        ~Porta_av();
      void abate_missil(); //Método para abater missil
    };
#endif