#include "mapa.hpp"
#include <iostream> 
#include <string>
using namespace std;

Mapa::Mapa(){
	cout<<"Mapa iniciada"<<endl;
	num_de_linhas = 0;
	num_de_colunas = 0;
}
Mapa::~Mapa(){
	cout<<"Mapa fechado"<<endl;
}
int Mapa::get_num_de_linhas(){
    return num_de_linhas;
}
void Mapa::set_num_de_linhas(int num_de_linhas){
    this->num_de_linhas = num_de_linhas;
}
int Mapa::get_num_de_colunas(){
    return num_de_colunas;
}
void Mapa::set_num_de_colunas(int num_de_colunas){
    this->num_de_colunas = num_de_colunas;
    }
    //para ler o arquivo txt que contem o mapa utilizar funcao "fopen"