#ifndef CANOA_HPP
#define CANOA_HPP
#include <string>
#include "barcos.hpp"

using namespace std;

class Canoa : private Barcos{ //classe filha numero 1
    public:
    int qntd_de_casas;
    int habilidade;    

    private: //atributo privado
    Canoa();
    ~Canoa();

};
#endif