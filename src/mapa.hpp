#ifndef MAPA_HPP
#define MAPA_HPP

class Mapa{
private:
	int num_de_linhas;
	int num_de_colunas;
public:
	Mapa();
	~Mapa();

	int get_num_de_linhas();
	void set_num_de_linhas(int num_de_linhas);
	int get_num_de_colunas();
	void set_num_de_colunas(int num_de_colunas);
};

#endif