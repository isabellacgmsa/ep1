#include "barcos.hpp"
#include <iostream> 
#include <string>

using namespace std;

Barcos::Barcos(){
	qntd_de_casas=0;
	habilidade = 0;
}
Barcos::~Barcos(){
	cout<<"Barco destruído\n"<<endl;
}
int Barcos::get_habilidade(){
    return habilidade;
}
void Barcos::set_habilidade(int habilidade){
    this->habilidade = habilidade;
}
int Barcos::get_qntd_de_casas(){
    return qntd_de_casas;
}
void Barcos::set_qntd_de_casas(int qntd_de_casas){
    this->qntd_de_casas = qntd_de_casas;
}