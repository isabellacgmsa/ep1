#ifndef BARCOS_HPP
#define BARCOS_HPP
#include <string>

using namespace std;

class Barcos{
private:
	int qntd_de_casas;
	int habilidade; // Para diferenciar as habilidades será adotado como:
public:             // 1- CANOA (sem habilidade), 2- SUBMARINO (atingir cada casa 2 x) 
	Barcos();       // 3-PORTA AV(Randomicamente abater missil)
	~Barcos();
	int get_qntd_de_casas();
	void set_qntd_de_casas(int qntd_de_casas);
	int get_habilidade();
	void set_habilidade(int habilidade);
};
#endif